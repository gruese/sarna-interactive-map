import { GSpreadsheet, SheetInfo, Era, Faction, System } from './types';
import { getA1Range } from './util';

const SHEETS_API_BASE_URL = 'https://sheets.googleapis.com/v4/spreadsheets';

export class SuckitReader {
  eras: Era[] = [];
  factions: Faction[] = [];
  systems: System[] = [];
  defaultParams: string;

  constructor(private spreadsheetId: string, private apiKey: string) {
    this.defaultParams = `$key=${apiKey}`;
  }

  async readSpreadsheetProperties(): Promise<GSpreadsheet> {
    const response = await fetch(`${SHEETS_API_BASE_URL}/${this.spreadsheetId}?${this.defaultParams}`);
    return (await response.json()) as GSpreadsheet;
  }

  findSheetInfo(spreadsheet: GSpreadsheet, sheetName: string): SheetInfo {
    const sheet = spreadsheet.sheets.find((s) => s.properties.title.trim().toLowerCase() === sheetName.toLowerCase());
    if (!sheet) {
      throw new Error(`The sheet named "${sheetName}" could not be found`);
    }
    return {
      sheetName: sheet.properties.title,
      fullRange: getA1Range(
        0,
        0,
        sheet.properties.gridProperties.rowCount,
        sheet.properties.gridProperties.columnCount,
      ),
    };
  }

  async readDataFromSheet(spreadsheet: GSpreadsheet, sheetName: string): Promise<any> {
    const sheetInfo = this.findSheetInfo(spreadsheet, sheetName);
    const url = [
      SHEETS_API_BASE_URL,
      this.spreadsheetId,
      'values',
      sheetInfo.sheetName + '!' + sheetInfo.fullRange,
    ].join('/');
    const response = await fetch(`${url}?${this.defaultParams}`);
    return await response.json();
  }

  async readEras(spreadsheet: GSpreadsheet) {
    this.eras = [];
    const data = await this.readDataFromSheet(spreadsheet, 'Systems-Description');
    data.values.forEach((row: string[]) => {
      if (row.length < 3) {
        return;
      }
      const year = parseInt(row[1]);
      if (year > 2000) {
        this.eras.push({
          year,
          description: row[2],
        });
      }
    });
  }

  async readFactions(spreadsheet: GSpreadsheet) {
    this.factions = [];
    const data = await this.readDataFromSheet(spreadsheet, 'Factions');
    data.values.forEach((row: string[]) => {
      if (!row[0] || !row[1] || row[0] === 'ID' || row[1] === 'factionName') {
        return;
      }
      this.factions.push({
        id: row[0].trim(),
        name: row[1].trim(),
        color: row[2].trim(),
      });
    });
  }

  getFactionById(factionId: string): Faction | void {
    return this.factions.find((faction) => faction.id.toUpperCase() === factionId.toUpperCase());
  }

  /**
   * Assumptions:
   * - Sheet is named "Systems"
   * - 2 header rows
   * - primary name in column 1 (B)
   * - alternate names in column 2 (C)
   * - x coordinate in column 3 (D)
   * - y coordinate in column 4 (E)
   * - sarna link in column 6 (G)
   * - era affiliations start at column 8 (I)
   * - number of eras is identical with era list in "Systems-Description" sheet
   * @param spreadsheet
   */
  async readSystems(spreadsheet: GSpreadsheet) {
    const ERA_AFFILIATIONS_START_COLUMN = 8;
    this.systems = [];
    const data = await this.readDataFromSheet(spreadsheet, 'Systems');
    data.values.forEach((row: string[], rowIndex: number) => {
      if (rowIndex < 2 || row.length < 2 || !row[1].trim()) {
        return;
      }
      const [, name, alternateNames, xCoordinate, yCoordinate, , sarnaLink] = row;
      const affiliations: string[] = [];
      for (let eraI = 0; eraI < this.eras.length; eraI++) {
        affiliations.push(
          (row[ERA_AFFILIATIONS_START_COLUMN + eraI] || '').split(',')[0],
        );
      }
      this.systems.push({
        name,
        eraNames: [],
        affiliations,
        sarnaLink,
        x: parseFloat(xCoordinate.replace(',', '')),
        y: parseFloat(yCoordinate.replace(',', '')),
      });
    });
  }

  async read() {
    const spreadsheet = await this.readSpreadsheetProperties();
    await this.readEras(spreadsheet);
    await this.readFactions(spreadsheet);
    await this.readSystems(spreadsheet);
  }
}
