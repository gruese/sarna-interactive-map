/**
 * see https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/sheets
 */
export interface GSingleSheet {
  properties: {
    sheetId: string;
    title: string;
    index: number;
    sheetType: 'GRID' | 'OBJECT' | 'DATA_SOURCE' | 'SHEET_TYPE_UNSPECIFIED';
    gridProperties?: {
      rowCount: number;
      columnCount: number;
      frozenRowCount?: number;
      frozenColumnCount?: number;
      rowGroupControlAfter?: boolean;
      columnGroupControlAfter?: boolean;
    }
  }
}

/**
 * see https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets
 */
export interface GSpreadsheet {
  properties: {
    title: string;
    locale: string;
    autoRecalc: 'ON_CHANGE' | 'MINUTE' | 'HOUR' | 'RECALCULATION_INTERVAL_UNSPECIFIED';
    timeZone: string;
    defaultFormat?: {
      backgroundColor?: {
        red: number;
        green: number;
        blue: number;
      };
      padding?: {
        right: number;
        left: number;
      };
      horizontalAlignment?: 'LEFT' | 'CENTER' | 'RIGHT' | 'HORIZONTAL_ALIGN_UNSPECIFIED';
      verticalAlignment?: 'TOP' | 'MIDDLE' | 'BOTTOM' | 'VERTICAL_ALIGN_UNSPECIFIED';
      wrapStrategy?: 'OVERFLOW_CELL' | 'LEGACY_WRAP' | 'CLIP' | 'WRAP' | 'WRAP_STRATEGY_UNSPECIFIED';
      // TODO complete this (https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets#spreadsheetproperties)
      //   or even better: get it from a google source
    };
  }
  sheets: GSingleSheet[];
}

export interface SheetInfo {
  sheetName: string;
  fullRange: string;
}

export interface Era {
  year: number;
  description: string;
}

export interface Faction {
  id: string;
  name: string;
  color: string;
}

export interface System {
  name: string;
  sarnaLink: string;
  eraNames: string[];
  affiliations: string[];
  x: number;
  y: number;
}
