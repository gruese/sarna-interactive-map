import { getA1Range, indexToColumn, rowColToA1 } from './util';

describe('util', () => {
  it('should convert column indices correctly', () => {
    expect(indexToColumn(0)).toBe('A');
    expect(indexToColumn(5)).toBe('F');
    expect(indexToColumn(26)).toBe('AA');
    expect(indexToColumn(30)).toBe('AE');
    expect(indexToColumn(702)).toBe('AAA');
    expect(indexToColumn(704)).toBe('AAC');
  });

  it('should convert row / column indices correctly', () => {
    expect(rowColToA1(23, 4)).toBe('E24');
    expect(rowColToA1(457, 31)).toBe('AF458');
  });

  it('should convert range indices correctly', () => {
    expect(getA1Range(0, 0, 23, 4)).toBe('A1:E24');
    expect(getA1Range(23, 4, 457, 31)).toBe('E24:AF458');
  });
});
