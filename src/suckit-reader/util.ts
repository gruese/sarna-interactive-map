export function indexToColumn(index: number): string {
  let aColumn = '';
  let remainingIndex = index;
  while (remainingIndex >= 0) {
    aColumn = String.fromCharCode((remainingIndex % 26) + 65) + aColumn;
    remainingIndex = Math.floor(remainingIndex / 26 - 1);
  }
  return aColumn || 'A';
}

export function rowColToA1(rowIndex: number, colIndex: number): string {
  return indexToColumn(colIndex) + (Math.floor(rowIndex) + 1);
}

export function getA1Range(
  rowStartIndex: number,
  colStartIndex: number,
  rowEndIndex: number,
  colEndIndex: number,
) {
  return rowColToA1(rowStartIndex, colStartIndex) + ':' + rowColToA1(rowEndIndex, colEndIndex);
}
