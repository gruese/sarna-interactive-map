import * as d3 from 'd3';
import { SuckitReader } from './suckit-reader';
import { System } from './suckit-reader/types';

const API_KEY = 'AIzaSyBMsqTAmOdCNy0EBF3-1qblt75qnEZMyIE';

window.onload = () => {
  const reader = new SuckitReader('1x9bvFqSb4_or8JbvGj2LnezGkChWxEzRPf5FXvjonHE', API_KEY);
  reader.read().then(() => {
    initRenderer(reader);
  });
};

function initRenderer(reader: SuckitReader) {
  console.log(window.innerWidth, window.innerHeight);
  new CanvasRenderer(reader);
}

export class CanvasRenderer {
  canvas: HTMLCanvasElement;
  canvasScalingFactor: number;
  context: CanvasRenderingContext2D;
  xScale: d3.ScaleLinear<number, number>;
  yScale: d3.ScaleLinear<number, number>;
  zoomFunction: d3.ZoomBehavior<Element, unknown>;

  height: number;
  width: number;

  constructor(private reader: SuckitReader) {
    this.canvasScalingFactor = window.devicePixelRatio;
    const container = document.querySelector('.main-container');
    const containerBbox = container.getBoundingClientRect();
    this.canvas = document.querySelector('canvas');
    this.canvas.height = containerBbox.height * this.canvasScalingFactor;
    this.canvas.width = containerBbox.width * this.canvasScalingFactor;
    this.canvas.style.height = containerBbox.height + 'px';
    this.canvas.style.width = containerBbox.width + 'px';
    const canvasBbox = this.canvas.getBoundingClientRect();

    this.context = this.canvas.getContext('2d');
    this.context.scale(this.canvasScalingFactor, this.canvasScalingFactor);

    // in memory only element
    // const detachedContainer = document.createElement('custom');

    // d3 selection
    // const dataContainer = d3.select(detachedContainer);

    const aspectRatio = canvasBbox.width / canvasBbox.height;
    let xMax = 800;
    let yMax = 800;
    if (aspectRatio < 1) {
      yMax /= aspectRatio;
    } else {
      xMax *= aspectRatio;
    }

    this.xScale = d3.scaleLinear()
      .domain([-xMax, xMax])
      .range([0, canvasBbox.width]);
    this.yScale = d3.scaleLinear()
      .domain([yMax, -yMax])
      .range([0, canvasBbox.height]);

    this.zoomFunction = d3
      .zoom()
      .scaleExtent([1, 80])
      .on('zoom', (event) => {
        this.context.save();
        this.draw(event.transform);
        this.context.restore();
      });

    // initial draw with no zoom
    this.draw();

    // zoom / drag handler
    d3.select(this.canvas).call(this.zoomFunction);
  }

  draw(transform = d3.zoomIdentity) {
    const bbox = this.canvas.getBoundingClientRect();
    this.width = bbox.width;
    this.height = bbox.height;
    this.xScale.range([0, this.width]);
    this.yScale.range([0, this.height]);

    const transformedXScale = transform.rescaleX(this.xScale);
    const transformedYScale = transform.rescaleY(this.yScale);

    this.context.clearRect(0, 0, this.width, this.height);

    // clear canvas
    this.context.fillStyle = '#f8f8f8';
    this.context.rect(0, 0, this.width, this.height);
    this.context.fill();

    this.reader.systems.forEach((dataPoint) => {
      this.drawPoint(dataPoint, transformedXScale, transformedYScale);
    });
  }

  drawPoint(system: System, xScale, yScale) {
    const faction = this.reader.getFactionById(system.affiliations[16]);
    this.context.beginPath();
    this.context.fillStyle = faction ? faction.color : '#eee';
    this.context.strokeStyle = 'black';
    this.context.arc(
      xScale(system.x),
      yScale(system.y),
      4,
      0,
      2 * Math.PI,
      false,
    );
    this.context.fill();
    this.context.stroke();
    this.context.closePath();
  }
}

// const zoomFunction = d3.zoom().scaleExtent([1, 10])
//   .on('zoom', (event) => {
//     const transform = event.transform;
//     draw(transform);
//   });
//
// const xScale = d3.scaleLinear()
//   .domain([-500, 500]);
// const yScale = d3.scaleLinear()
//   .domain([-500, 500]);
//
// function draw(
//   context: CanvasRenderingContext2D,
//
//   transform: d3.ZoomTransform) {
//   const transformedXScale = transform.rescaleX(xScale);
//   const transformedYScale = transform.rescaleY(yScale);
//
//   context.clearRect(0, 0, width, height)
// }

// function drawPoints(data: Point2d[]) {
//   const canvasScalingFactor = window.devicePixelRatio;
//   const container = document.querySelector('.main-container');
//   const containerBbox = container.getBoundingClientRect();
//   const canvas = document.querySelector('canvas');
//   canvas.height = containerBbox.height * canvasScalingFactor;
//   canvas.width = containerBbox.width * canvasScalingFactor;
//   canvas.style.height = containerBbox.height + 'px';
//   canvas.style.width = containerBbox.width + 'px';
//   const canvasBbox = canvas.getBoundingClientRect();
//
//   const context = canvas.getContext('2d');
//   context.scale(canvasScalingFactor,canvasScalingFactor);
//
//   // in memory only element
//   const detachedContainer = document.createElement('custom');
//
//   // d3 selection
//   const dataContainer = d3.select(detachedContainer);
//
//   console.log(canvasBbox.width);
//   xScale.range([0, canvasBbox.width]);
//   yScale.range([0, canvasBbox.height]);
//
//   dataContainer
//     .selectAll('custom.circle')
//     .data(data, (_, i) => i)
//     .join(
//       (enter) => {
//         return enter
//           .append('custom')
//           .classed('circle', true)
//           .attr('cx', (d) => xScale(d.x))
//           .attr('cy', (d) => yScale(d.y))
//           .attr('r', 4)
//           .attr('fillStyle', 'red');
//       },
//       (update) => update,
//       (exit) => exit.remove(),
//     );
//
//   drawCanvas(context, dataContainer);
// }
//
// function drawCanvas(context: CanvasRenderingContext2D, dataContainer: any) {
//   const canvas = document.querySelector('canvas');
//   const bbox = canvas.getBoundingClientRect();
//
//   // clear canvas
//   context.fillStyle = '#f8f8f8';
//   context.rect(0, 0, bbox.width,  bbox.height);
//   context.fill();
//
//   const elements = dataContainer.selectAll('custom.circle');
//   elements.each(function () {
//     drawPoint(context, d3.select(this))
//   });
// }
//
// function drawPoint(context: CanvasRenderingContext2D, node: d3.Selection<any, unknown, null, undefined>) {
//   context.beginPath();
//   context.fillStyle = node.attr('fillStyle');
//   context.strokeStyle = 'black';
//   context.arc(
//     parseFloat(node.attr('cx')),
//     parseFloat(node.attr('cy')),
//     parseFloat(node.attr('r')),
//     0,
//     2 * Math.PI,
//     false
//   );
//   context.fill();
//   context.stroke();
//   context.closePath();
// }
